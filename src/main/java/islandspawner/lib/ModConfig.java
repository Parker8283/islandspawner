package islandspawner.lib;

import islandspawner.item.ShelterInfo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import net.minecraft.block.Block;
import net.minecraftforge.common.config.Configuration;

public class ModConfig {

    private static final String CATEGORY_HOUSE = "HOUSE.HOUSE";
    private static final String CATEGORY_ROOF = "HOUSE.ROOF";
    private static final String CATEGORY_BEAMS = "HOUSE.BEAMS";
    private static final String CATEGORY_DIRT = "HOUSE.DIRT";
    private static final String CATEGORY_GRASS = "HOUSE.GRASS";
    private static final String CATEGORY_WATER = "HOUSE.WATER";
    private static final String CATEGORY_LIGHT = "HOUSE.LIGHT";
    private static final String CATEGORY_FENCES = "HOUSE.FENCES";

    public static void init(File file) {
        Configuration config = new Configuration(file);

        config.load();

        //HOUSE
        ShelterInfo.HOUSE_BLOCK = Block.getBlockFromName(config.get(CATEGORY_HOUSE, ShelterInfo.HOUSE_BLOCK_KEY, ShelterInfo.HOUSE_BLOCK_DEFAULT).getString());
        ShelterInfo.HOUSE_META = config.get(CATEGORY_HOUSE, ShelterInfo.HOUSE_META_KEY, ShelterInfo.HOUSE_META_DEFAULT).getInt();
        //ROOF
        ShelterInfo.ROOF_BLOCK = Block.getBlockFromName(config.get(CATEGORY_ROOF, ShelterInfo.ROOF_BLOCK_KEY, ShelterInfo.ROOF_BLOCK_DEFAULT).getString());
        ShelterInfo.ROOF_META = config.get(CATEGORY_ROOF, ShelterInfo.ROOF_META_KEY, ShelterInfo.ROOF_META_DEFAULT).getInt();
        //BEAMS
        ShelterInfo.BEAMS_BLOCK = Block.getBlockFromName(config.get(CATEGORY_BEAMS, ShelterInfo.BEAMS_BLOCK_KEY, ShelterInfo.BEAMS_BLOCK_DEFAULT).getString());
        ShelterInfo.BEAMS_META = config.get(CATEGORY_BEAMS, ShelterInfo.BEAMS_META_KEY, ShelterInfo.BEAMS_META_DEFAULT).getInt();
        //DIRT
        ShelterInfo.DIRT_BLOCK = Block.getBlockFromName(config.get(CATEGORY_DIRT, ShelterInfo.DIRT_BLOCK_KEY, ShelterInfo.DIRT_BLOCK_DEFAULT).getString());
        ShelterInfo.DIRT_META = config.get(CATEGORY_DIRT, ShelterInfo.DIRT_META_KEY, ShelterInfo.DIRT_META_DEFAULT).getInt();
        //GRASS
        ShelterInfo.GRASS_BLOCK = Block.getBlockFromName(config.get(CATEGORY_GRASS, ShelterInfo.GRASS_BLOCK_KEY, ShelterInfo.GRASS_BLOCK_DEFAULT).getString());
        ShelterInfo.GRASS_META = config.get(CATEGORY_GRASS, ShelterInfo.GRASS_META_KEY, ShelterInfo.GRASS_META_DEFAULT).getInt();
        //WATER
        ShelterInfo.WATER_BLOCK = Block.getBlockFromName(config.get(CATEGORY_WATER, ShelterInfo.WATER_BLOCK_KEY, ShelterInfo.WATER_BLOCK_DEFAULT).getString());
        ShelterInfo.WATER_META = config.get(CATEGORY_WATER, ShelterInfo.WATER_META_KEY, ShelterInfo.WATER_META_DEFAULT).getInt();
        //LIGHT
        ShelterInfo.LIGHT_BLOCK = Block.getBlockFromName(config.get(CATEGORY_LIGHT, ShelterInfo.LIGHT_BLOCK_KEY, ShelterInfo.LIGHT_BLOCK_DEFAULT).getString());
        ShelterInfo.LIGHT_META = config.get(CATEGORY_LIGHT, ShelterInfo.LIGHT_META_KEY, ShelterInfo.LIGHT_META_DEFAULT).getInt();
        //FENCES
        ShelterInfo.FENCES_BLOCK = Block.getBlockFromName(config.get(CATEGORY_FENCES, ShelterInfo.FENCES_BLOCK_KEY, ShelterInfo.FENCES_BLOCK_DEFAULT).getString());
        ShelterInfo.FENCES_META = config.get(CATEGORY_FENCES, ShelterInfo.FENCES_META_KEY, ShelterInfo.FENCES_META_DEFAULT).getInt();

        if(config.hasChanged())
        	config.save();
    }
    
    public static void writeBlockNames(File file) {
    	PrintWriter writer = null;
    	
    	try {
			writer = new PrintWriter(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    	
    	writer.println("####################");
    	writer.println("INFO");
    	writer.println("####################");
    	writer.println();
    	writer.println("This file contains the vanilla block names that should be used in the configuration file.");
    	writer.println();
    	writer.println("####################");
    	writer.println("BLOCK NAMES");
    	writer.println("####################");
    	writer.println();
    	
    	for(int i = 0; i < 176; i++) {
    		Block block = Block.getBlockById(i);
    		writer.println(block.getLocalizedName() + " = " + Block.blockRegistry.getNameForObject(block));
    	}
    	writer.close();
    }
}
