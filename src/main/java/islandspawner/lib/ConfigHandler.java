package islandspawner.lib;

import java.io.File;

public class ConfigHandler {

    public static void init(String configPath)
    {
        ModConfig.init(new File(configPath + "islandbuilder.cfg"));
        ModConfig.writeBlockNames(new File(configPath + "blockNames.txt"));
    }
}
