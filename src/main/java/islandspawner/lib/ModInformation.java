package islandspawner.lib;

public class ModInformation {
	
	public static final String MODID = "islandbuilder";
	public static final String MOD_NAME = "Island Builder";
	public static final String VERSION = "@MOD_VERSION@";
	public static final String CHANNEL = "islandspawner";
    public static final String CONFIG_LOC_NAME = "IslandBuilder";

}
