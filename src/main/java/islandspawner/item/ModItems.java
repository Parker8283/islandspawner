package islandspawner.item;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;

public class ModItems {

	public static Item shelter;

	public static void init() {
        shelter = new Shelter();
        GameRegistry.registerItem(shelter, ItemInfo.SHELTER_KEY);
    }
}
