package islandspawner.item;

import islandspawner.IslandSpawner;
import islandspawner.lib.ModInformation;

import java.util.List;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class Shelter extends Item {

    public Shelter() {
        this.setCreativeTab(IslandSpawner.stTab);
        this.setMaxStackSize(1);
        this.setMaxDamage(1);
        this.setUnlocalizedName("shelter");
    }

    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister par1IconRegister){
        this.itemIcon = par1IconRegister.registerIcon(ModInformation.MODID + ":" + ItemInfo.SHELTER_TEXTURES);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
    public void addInformation(ItemStack item, EntityPlayer player, List lst, boolean extraInfo)
    {
        super.addInformation(item, player, lst, extraInfo);

        lst.add("Sneak right-click for an island to spawn.");
    }

    public boolean onItemUseFirst(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int par7, float par8, float par9, float par10)
    {
            if (!world.isRemote && player.isSneaking()) {
                IslandSpawner.log.info("Started Building");
                generate(world, x, y, z);
                IslandSpawner.log.info("Finished Building");
                if (!player.capabilities.isCreativeMode) {
                    --stack.stackSize;
                }
            }
        return false;
    }

    public boolean generate(World world, int x, int y, int z)
    {
        new IslandSpawner1().generate(world, x, y, z);
        new IslandSpawner2().generate(world, x, y, z);
        new IslandSpawner3().generate(world, x, y, z);
        new IslandSpawner4().generate(world, x, y, z);
        new IslandSpawner5().generate(world, x, y, z);
        new IslandSpawner6().generate(world, x, y, z);
        new IslandSpawner7().generate(world, x, y, z);
        new IslandSpawner8().generate(world, x, y, z);
        new IslandSpawner9().generate(world, x, y, z);
        new IslandSpawner10().generate(world, x, y, z);
        return true;
    }

}