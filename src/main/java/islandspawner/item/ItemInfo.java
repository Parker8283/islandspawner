package islandspawner.item;

public class ItemInfo {

	//Shelter
	public static final String SHELTER_KEY = "Shelter";
	public static final String SHELTER_UNLOCALIZED_NAME = "shelter";
	public static final String SHELTER_TEXTURES = "Shelter";
}
