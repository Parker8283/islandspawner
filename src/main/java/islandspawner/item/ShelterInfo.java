package islandspawner.item;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;

public class ShelterInfo {

    public static Block HOUSE_BLOCK;
    public static final String HOUSE_BLOCK_DEFAULT = Block.blockRegistry.getNameForObject(Blocks.stained_hardened_clay);
    public static final String HOUSE_BLOCK_KEY = "Block name for the walls of the house";

    public static int HOUSE_META;
    public static final int HOUSE_META_DEFAULT = 0;
    public static final String HOUSE_META_KEY = "Meta ID for the walls of the house";

    public static Block ROOF_BLOCK;
    public static final String ROOF_BLOCK_DEFAULT = Block.blockRegistry.getNameForObject(Blocks.stained_hardened_clay);
    public static final String ROOF_BLOCK_KEY = "Block name for the roof of the house";

    public static int ROOF_META;
    public static final int ROOF_META_DEFAULT = 4;
    public static final String ROOF_META_KEY = "Meta ID for the roof of the house";

    public static Block BEAMS_BLOCK;
    public static final String BEAMS_BLOCK_DEFAULT = Block.blockRegistry.getNameForObject(Blocks.stained_hardened_clay);
    public static final String BEAMS_BLOCK_KEY = "Block name for the beams of the house";

    public static int BEAMS_META;
    public static final int BEAMS_META_DEFAULT = 7;
    public static final String BEAMS_META_KEY = "Meta ID for the beams of the house";

    public static Block GRASS_BLOCK;
    public static final String GRASS_BLOCK_DEFAULT = Block.blockRegistry.getNameForObject(Blocks.stained_hardened_clay);
    public static final String GRASS_BLOCK_KEY = "Block name for the grass next the house";

    public static int GRASS_META;
    public static final int GRASS_META_DEFAULT = 13;
    public static final String GRASS_META_KEY = "Meta ID for the grass next the house";

    public static Block DIRT_BLOCK;
    public static final String DIRT_BLOCK_DEFAULT = Block.blockRegistry.getNameForObject(Blocks.stained_hardened_clay);
    public static final String DIRT_BLOCK_KEY = "Block name for the dirt below the grass";

    public static int DIRT_META;
    public static final int DIRT_META_DEFAULT = 12;
    public static final String DIRT_META_KEY = "Meta ID for the dirt below the grass";

    public static Block WATER_BLOCK;
    public static final String WATER_BLOCK_DEFAULT = Block.blockRegistry.getNameForObject(Blocks.stained_hardened_clay);
    public static final String WATER_BLOCK_KEY = "Block name for the waterway";

    public static int WATER_META;
    public static final int WATER_META_DEFAULT = 3;
    public static final String WATER_META_KEY = "Meta ID for the waterway";

    public static Block LIGHT_BLOCK;
    public static final String LIGHT_BLOCK_DEFAULT = Block.blockRegistry.getNameForObject(Blocks.lit_pumpkin);
    public static final String LIGHT_BLOCK_KEY = "Block name for the lights on the island";

    public static int LIGHT_META;
    public static final int LIGHT_META_DEFAULT = 0;
    public static final String LIGHT_META_KEY = "Meta ID for the lights on the island";

    public static Block FENCES_BLOCK;
    public static final String FENCES_BLOCK_DEFAULT = Block.blockRegistry.getNameForObject(Blocks.fence);
    public static final String FENCES_BLOCK_KEY = "Block name for the fences";

    public static int FENCES_META;
    public static final int FENCES_META_DEFAULT = 0;
    public static final String FENCES_META_KEY = "Meta ID for the fences";
}
