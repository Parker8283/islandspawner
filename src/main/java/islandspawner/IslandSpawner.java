package islandspawner;

import islandspawner.item.ModItems;
import islandspawner.lib.ConfigHandler;
import islandspawner.lib.ModInformation;
import islandspawner.network.ChannelHandler;
import islandspawner.network.ModNetwork;
import islandspawner.proxy.CommonProxy;

import java.io.File;

import net.minecraft.creativetab.CreativeTabs;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartedEvent;
import cpw.mods.fml.common.network.NetworkRegistry;

@Mod(modid = ModInformation.MODID , name = ModInformation.MOD_NAME , version = ModInformation.VERSION)
public class IslandSpawner extends ModNetwork {
	  
	@Instance(ModInformation.MODID)
	public static IslandSpawner instance;
	
	@SidedProxy(clientSide = "islandspawner.proxy.ClientProxy", serverSide = "islandspawner.proxy.CommonProxy")
	public static CommonProxy proxy;

    public static String path;
    public static Logger log;
	public static CreativeTabs stTab = new ISBTab("stTab");

	@EventHandler 
	public void preInit(FMLPreInitializationEvent event) {
		log = LogManager.getLogger(ModInformation.MOD_NAME);
        path = event.getModConfigurationDirectory().getAbsolutePath() + File.separator + ModInformation.CONFIG_LOC_NAME.toLowerCase() + File.separator;
        ConfigHandler.init(path);
		ModItems.init();
	}
	
	@EventHandler 
	public void load(FMLInitializationEvent event) {
		channels = NetworkRegistry.INSTANCE.newChannel(ModInformation.CHANNEL, new ChannelHandler());
	}
	
	
	@EventHandler 
	public void PostInit(FMLPostInitializationEvent event) {

	}
	
	@EventHandler
	public void Serverstart(FMLServerStartedEvent event) {
		
	}
}
